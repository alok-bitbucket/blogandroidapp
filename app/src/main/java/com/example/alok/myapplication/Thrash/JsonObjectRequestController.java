package com.example.alok.myapplication.Thrash;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.alok.myapplication.controller.ServerRequestController;

import org.json.JSONObject;

/**
 * Created by Alok on 06-May-17.
 */

public class JsonObjectRequestController {
    private String TAG = JsonObjectRequestController.class.getSimpleName();

    public void volleyJsonObjectRequest(Context context, String url){

        JsonObjectRequest jsonObjectReq = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "jsonObjectReq response "+ response.toString());


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "jsonObjectReq response "+ error.getMessage());
            }
        });

        // Adding JsonObject request to request queue
        ServerRequestController.getInstance(context).addToRequestQueue(jsonObjectReq,url);
    }
}
