package com.example.alok.myapplication.controller;

import android.content.Context;
import android.icu.util.Measure;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.alok.myapplication.utils.Constant;
import com.example.alok.myapplication.model.Model;
import com.example.alok.myapplication.volleyUtils.CustomRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alok on 14-May-17.
 */

public class WriteController {
    private String TAG =  ReadController.class.getSimpleName();

    public void submitWriteRequest(Context context, Model model, String url){
        Log.d(TAG,"submitWriteRequest");
        switch(model.RequestResourceType){

            case Constant.STRING_TYPE:
                volleyStringRequest(context,model,url);
                break;
            case Constant.JSONOBJECT_TYPE:
                volleyJsonObjectRequest(context,model,url);
                break;
            case Constant.IMAGE_TYPE:
                //Not supported, Checking further
                break;
            case Constant.JSONARRAY_TYPE:
                //Not supported, Checking further
                break;
            default:
        }
    }

    private void volleyStringRequest(Context context, Model model, String url){
        String  tag_string_req = "string_req_write";
        StringRequest stringRequest = new StringRequest(Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG,"onStringResponse: true");
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG,"onStringResponse: false");
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                /*
                param.put("key","value");
                */
                return params;
            }
        };

        ServerRequestController.getInstance(context).addToRequestQueue(stringRequest,tag_string_req);
    }

    private void volleyJsonObjectRequest(Context context, Model model,String url){
        String  tag_json_req = "json_req_write";
        HashMap<String,String> param = new HashMap<>();
        /*
        param.put("key","value");
        */
        CustomRequest jsonObjectReq = new CustomRequest(Method.POST, url, param, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        // Adding JsonObject request to request queue
        ServerRequestController.getInstance(context).addToRequestQueue(jsonObjectReq,tag_json_req);

    }

    /*    private void volleyImageRequest(Context context, Model model,String url){
        ImageLoader imageLoader = ServerRequestController.getInstance(context).getImageLoader();

        imageLoader.get(url, new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Image Load Error: " + error.getMessage());
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                Log.e(TAG, "Image Load Success");
                if (response.getBitmap() != null) {
                    //Write logic
                }
            }
        });
    }

    private void volleyJsonArrayRequest(Context context, Model model,String url){
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, "jsonObjectReq response "+ response.toString());


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "jsonObjectReq response "+ error.getMessage());
            }
        });

        // Adding JsonObject request to request queue
        ServerRequestController.getInstance(context).addToRequestQueue(jsonArrayRequest,url);

    }*/
}
