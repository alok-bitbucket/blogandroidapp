package com.example.alok.myapplication.controller;

import android.content.Context;
import android.util.Log;

import com.example.alok.myapplication.utils.BlogException;
import com.example.alok.myapplication.utils.Constant;
import com.example.alok.myapplication.model.Model;
import com.example.alok.myapplication.utils.Utils;

/**
 * Created by Alok on 14-May-17.
 */

public class ClientController {
    private String TAG = ClientController.class.getSimpleName();
    private String ServerUrl;


    public void submitRequest(Context context, Model model) throws BlogException{
        Log.d(TAG,"Submit Request");

        String url = Utils.getUrl();

        if(model.RequestActionType == Constant.READ){
            ReadController rc = new ReadController();
            rc.submitReadRequest(context,model,url);
        }else if(model.RequestActionType == Constant.WRITE){
            WriteController wc = new WriteController();
            wc.submitWriteRequest(context,model,url);
        }else if(model.RequestActionType == Constant.UPDATE){
            //For update
        }else{
            //For Exceptional case
        }
    }
}
