package com.example.alok.myapplication.Thrash;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.alok.myapplication.controller.ServerRequestController;

import org.json.JSONArray;

/**
 * Created by Alok on 06-May-17.
 */

public class JsonArrayRequestController {
    private String TAG = JsonArrayRequestController.class.getSimpleName();

    public void volleyJsonArrayRequest(Context context, String url){

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, "jsonObjectReq response "+ response.toString());


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "jsonObjectReq response "+ error.getMessage());
            }
        });

        // Adding JsonObject request to request queue
        ServerRequestController.getInstance(context).addToRequestQueue(jsonArrayRequest,url);
    }
}
