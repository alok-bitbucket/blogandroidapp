package com.example.alok.myapplication.model;

import org.json.JSONObject;

/**
 * Created by Alok on 14-May-17.
 */

public abstract class Model {
    public int RequestActionType;
    public int RequestResourceType;

    public void setRequestActionType(int requestActionType) {
    }

    public void setRequestResourceType(int requestResourceType) {
    }

    // Derived class will provide its definition
    public abstract JSONObject toJson();

}
