package com.example.alok.myapplication.utils;

/**
 * Created by Alok on 14-May-17.
 */

public class Constant {
    public static final int READ =0;
    public static final int WRITE =1;
    public static final int UPDATE =2;

    public static final int STRING_TYPE =0;
    public static final int IMAGE_TYPE =1;
    public static final int JSONARRAY_TYPE =2;
    public static final int JSONOBJECT_TYPE =3;


}
