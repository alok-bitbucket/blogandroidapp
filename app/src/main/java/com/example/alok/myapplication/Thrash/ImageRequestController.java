package com.example.alok.myapplication.Thrash;

import android.content.Context;
import android.util.Log;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.example.alok.myapplication.controller.ServerRequestController;

/**
 * Created by Alok on 06-May-17.
 */

public class ImageRequestController {
    private String TAG = ImageRequestController.class.getSimpleName();

    public void volleyImageLoader(Context context, String url){
        ImageLoader imageLoader = ServerRequestController.getInstance(context).getImageLoader();

        imageLoader.get(url, new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Image Load Error: " + error.getMessage());
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                Log.e(TAG, "Image Load Success");
                if (response.getBitmap() != null) {
                    //Write logic
                }
            }
        });
    }
}

