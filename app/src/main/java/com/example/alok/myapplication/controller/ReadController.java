package com.example.alok.myapplication.controller;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.alok.myapplication.utils.BlogException;
import com.example.alok.myapplication.utils.Constant;
import com.example.alok.myapplication.model.Model;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Alok on 14-May-17.
 */

public class ReadController {
    private String TAG =  ReadController.class.getSimpleName();

    public void submitReadRequest(Context context, Model model,String url) throws BlogException{
        Log.d(TAG,"submitReadRequest");
        switch(model.RequestResourceType){

            case Constant.STRING_TYPE:
                volleyStringRequest(context,model,url);
                break;
            case Constant.IMAGE_TYPE:
                volleyImageRequest(context,model,url);
                 break;
            case Constant.JSONARRAY_TYPE:
                volleyJsonArrayRequest(context,model,url);
                break;
            case Constant.JSONOBJECT_TYPE:
                volleyJsonObjectRequest(context,model,url);
                break;
            default:
                throw new BlogException("Submit Proper Resource Action type");
        }
    }

    private void volleyStringRequest(Context context, Model model, String url){
        String  tag_string_req = "string_req";
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG,"onStringResponse: true");
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG,"onStringResponse: false");
            }
        });

        ServerRequestController.getInstance(context).addToRequestQueue(stringRequest,tag_string_req);
    }

    private void volleyImageRequest(Context context, Model model,String url){
        ImageLoader imageLoader = ServerRequestController.getInstance(context).getImageLoader();

        imageLoader.get(url, new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Image Load Error: " + error.getMessage());
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                Log.e(TAG, "Image Load Success");
                if (response.getBitmap() != null) {
                    //Write logic
                }
            }
        });
    }

    private void volleyJsonObjectRequest(Context context, Model model,String url){
        JsonObjectRequest jsonObjectReq = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "jsonObjectReq response "+ response.toString());


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "jsonObjectReq response "+ error.getMessage());
            }
        });

        // Adding JsonObject request to request queue
        ServerRequestController.getInstance(context).addToRequestQueue(jsonObjectReq,url);

    }

    private void volleyJsonArrayRequest(Context context, Model model,String url){
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, "jsonObjectReq response "+ response.toString());


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "jsonObjectReq response "+ error.getMessage());
            }
        });

        // Adding JsonObject request to request queue
        ServerRequestController.getInstance(context).addToRequestQueue(jsonArrayRequest,url);

    }
}
