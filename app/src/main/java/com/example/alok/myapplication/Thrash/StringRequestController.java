package com.example.alok.myapplication.Thrash;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.alok.myapplication.controller.ServerRequestController;
import com.example.alok.myapplication.view.MainActivity;

/**
 * Created by Alok on 06-May-17.
 */

public class StringRequestController {

    private String TAG = StringRequestController.class.getSimpleName();

    public void submitStringRequest(Context context, String url, final MainActivity callabck){
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG,"onStringResponse: true");
                callabck.updateResponse(response);
            }
        }, new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG,"onStringResponse: false");
                callabck.updateResponse("I am not well "+ error.getMessage());
            }
        });

        ServerRequestController.getInstance(context).addToRequestQueue(stringRequest,url);
    }
}
