package com.example.alok.myapplication.view;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.alok.myapplication.R;
import com.example.alok.myapplication.controller.ClientController;
import com.example.alok.myapplication.model.Model;

public class MainActivity extends AppCompatActivity {

    TextView tv ;
    Handler mHandler;
    private String TAG = MainActivity.class.getSimpleName();
    private String url = "http://192.168.1.2:8080/home";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv = (TextView)findViewById(R.id.hello);

        mHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                Log.d(TAG, "handlemsg");
                super.handleMessage(msg);
                String str = msg.obj.toString();
                if(str!=null)
                    tv.setText(str);
            }
        };
    }

    public void updateResponse(String str){
        Log.d(TAG, "updateResponse");
        if(str!=null)
            tv.setText(str);
    }

    @Override
    protected void onResume() {
        super.onResume();
       // new StringRequestController().submitStringRequest(this,url,this);

        //for testing
        Model model = null;
        new ClientController().submitRequest(this,model,url);

    }
}
